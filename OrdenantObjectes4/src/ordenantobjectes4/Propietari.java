/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenantobjectes4;


import java.util.HashSet;
import java.util.Objects;

/**
 *
 * @author Ferran
 */
public class Propietari {
    private String dni; 
    private String nom;
    private String primerCognom;
    private String segonCognom;
    private HashSet<Gos> gossos; 
    private char sexe; 
    private int edat;

    public Propietari(String dni, String nom, String primerCognom, String segonCognom, HashSet<Gos> gossos, char sexe, int edat) {
        this.dni = dni;
        this.nom = nom;
        this.primerCognom = primerCognom;
        this.segonCognom = segonCognom;
        this.gossos = gossos;
        this.sexe = sexe;
        this.edat = edat;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        
        sb.append("DNI: ").append(dni).append(" nom: ").append(nom).append(" 1r cognom: ").append(primerCognom)
                .append(" 2n cognom: ").append(segonCognom).append(" sexe: ").append(sexe).append(" edat: ").append(edat);
        
        return sb.toString();
               
    }
    
    
//    @Override
//    public int hashCode(){
//        int hash = 37;
//        hash = 173 * hash + this.nom.hashCode();
//        hash = 173 * hash + this.primerCognom.hashCode();
//        hash = 173 * hash + this.segonCognom.hashCode();
//        return hash;
//    }
//    
//    @Override
//    public boolean equals(Object o){
//        if(o == this){
//            return false;
//        }
//        else if(o == null || o.getClass() != this.getClass()){
//            return false;
//        }
//        else{
//            Propietari p = (Propietari) o;
//            if(this.nom.equals(p.nom)){
//                if(this.primerCognom.equals(p.primerCognom)){
//                    if(this.segonCognom.equals(p.segonCognom)){
//                        return true;
//                    }
//                }
//            }
//        }
//        return false;
//    }
    
    
//    @Override
//    public int hashCode(){
//        int hash = 37;
//        hash = 173 * hash + this.nom.hashCode();
//        hash = 173 * hash + this.gossos.size();
//        
//        return hash;
//    }
//    
//    @Override
//    public boolean equals(Object o){
//        if(o == this){
//            return false;
//        }
//        else if(o == null || o.getClass() != this.getClass()){
//            return false;
//        }
//        else{
//            Propietari p = (Propietari) o;
//            if(this.nom.equals(p.nom)){
//                if(this.gossos.size() == p.gossos.size()){
//                    return true;
//                }
//            }
//        }
//        return false;
//    }

    
    //IMPLEMENTACIO 3
//    @Override
//    public int hashCode() {
//        int hash = 5;
//        hash = 89 * hash + Objects.hashCode(this.nom);
//        hash = 89 * hash + Objects.hashCode(this.primerCognom);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final Propietari other = (Propietari) obj;
//        if (!Objects.equals(this.nom, other.nom)) {
//            return false;
//        }
//        if (!Objects.equals(this.primerCognom, other.primerCognom)) {
//            return false;
//        }
//        return true;
//    }

    
    //IMPLEMENTACIO 4
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.dni.substring(0, this.dni.length()-1));
        hash = 79 * hash + Objects.hashCode(this.primerCognom);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Propietari other = (Propietari) obj;
        if (!Objects.equals(this.dni.substring(0, this.dni.length()-1), other.dni.substring(0, other.dni.length()-1))) {
            return false;
        }
        if (!Objects.equals(this.primerCognom, other.primerCognom)) {
            return false;
        }
        return true;
    }
    
    
    
    
    
}
