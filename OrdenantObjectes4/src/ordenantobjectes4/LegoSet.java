/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenantobjectes4;

import java.util.Date;

/**
 *
 * @author Ferran
 */
public class LegoSet implements Comparable<LegoSet>{
    private int codiSet; 
    private String nomSet; 
    private int dificultat; 
    private int numPeces; 
    private double preu; 
    private String categoria; 
    private Date dataLlansament; 

    public LegoSet(int codiSet, String nomSet, int dificultat, int numPeces, double preu, String categoria, Date dataLlansament) {
        this.codiSet = codiSet;
        this.nomSet = nomSet;
        this.dificultat = dificultat;
        this.numPeces = numPeces;
        this.preu = preu;
        this.categoria = categoria;
        this.dataLlansament = dataLlansament;
    }
    
    
    
    @Override
    public int hashCode(){
        int hash = 37;
        hash = 173 * hash + this.nomSet.hashCode();
        hash = 173 * hash + this.dataLlansament.hashCode();
        return hash;
    }
    
    @Override
    public boolean equals(Object o){
        if(o == this){
            return false;
        }
        else if(o == null || o.getClass() != this.getClass()){
            return false;
        }
        else{
            LegoSet l = (LegoSet) o; 
            return l.nomSet.equals(this.nomSet) && l.dataLlansament.equals(this.dataLlansament);
        }         
    }

    
    //retorna un int negatiu si el "this" es mes petit que o, un 0 si son iguals, i un positiu si o es mes petit
//    @Override
//    public int compareTo(LegoSet o) {
//        return codiSet - o.codiSet;
//        
//    }
    
//    @Override
//    public int compareTo(LegoSet o) {
//        return numPeces - o.numPeces;
//        
//    }
    
    @Override
    public int compareTo(LegoSet o){
        int num; 
        num = nomSet.compareTo(o.nomSet); 
        if(num ==0){
            num = dataLlansament.compareTo(o.dataLlansament);
        }
        
        return num;
    }
    //Dona problemes perque algunes coleccions funcionen quan el compareTo i el equals pero altres collections no funcionen, s'hauria de comparar amb la mateixa variable que el equals;
    
    
   
    

    @Override
    public String toString() {
        return "LegoSet{" + "codiSet=" + codiSet + ", nomSet=" + nomSet + ", dificultat=" + dificultat + ", numPeces=" + numPeces + ", preu=" + preu + ", categoria=" + categoria + ", dataLlansament=" + dataLlansament + '}';
    }

    public int getDificultat() {
        return dificultat;
    }

    public String getCategoria() {
        return categoria;
    }

    public String getNomSet() {
        return nomSet;
    }

    public int getNumPeces() {
        return numPeces;
    }

    public Date getDataLlansament() {
        return dataLlansament;
    }
    
    
    
    
}
