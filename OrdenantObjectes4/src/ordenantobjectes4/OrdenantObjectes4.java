/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenantobjectes4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * @author Ferran
 */
public class OrdenantObjectes4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Proves
        HashSet<Gos> hashGossosProves = new HashSet<>();
        Gos g = new Gos("123", "Francisco alberto tercero", "pepe", "mixte", true, new Date());
        Gos g2 = new Gos("1231", "qefqefq", "pepe2", "mixte2", true, new Date());
        Gos g3 = new Gos("1212e", "Antoni marti", "toni", "yorkshire", true, new Date());
        Gos g4 = new Gos("efwef13", "Marc marina", "toni2", "yorkshire", false, new Date());
        Gos g5 = new Gos("342342", "Oriol Barrera primero", "Uri", "German Shepherd", false, new Date());
        
        hashGossosProves.add(g);
        hashGossosProves.add(g2);
        
//        for(Gos o: hashGossos){
//            System.out.println(o.getCodiXip());
//        }
        
        //System.out.println(g.equals(g2));
        
        
        //Exercici 6
        HashSet<Gos> hashSetGossos = new HashSet<>();
        hashSetGossos.add(g);
        hashSetGossos.add(g2);
        hashSetGossos.add(g3);
        hashSetGossos.add(g4);
        hashSetGossos.add(g5);
        System.out.println(hashSetGossos.size());
        
        
        //proves amb la implementacio 3 de equals i hashCode de propietari
        Propietari p = new Propietari("39441139A","Ferran", "Lopez", "Puig", hashSetGossos, 'M', 22);
        Propietari p2 = new Propietari("39441139B","Antoni", "Marti", "Puig", hashSetGossos, 'F', 22);
        Propietari p3 = new Propietari("39441139C","Ferran", "Lopez", "Puig", hashSetGossos, 'X', 19);
        Propietari p4 = new Propietari("39441139D","Marc", "Marina", "Puig", hashSetGossos, 'M', 20 );
        
        
        HashSet<Propietari> hashPropietaris = new HashSet<>();
        
        hashPropietaris.add(p);
        hashPropietaris.add(p2);
        hashPropietaris.add(p3);
        hashPropietaris.add(p4);
        
        System.out.println("Implementacio 3");
        for(Propietari pro: hashPropietaris){
            System.out.println(pro);
        }
        
        
        //proves amb la implementacio 4 de equals i hashCode de propietari
        
        Propietari p5 = new Propietari("39441139A","Ferran", "Lopez", "Puig", hashSetGossos, 'M', 22);
        Propietari p6 = new Propietari("39441139B","Antoni", "Marti", "Puig", hashSetGossos, 'F', 22);
        Propietari p7 = new Propietari("39441139C","Ferran", "Lopez", "Puig", hashSetGossos, 'X', 19);
        Propietari p8 = new Propietari("39441139D","Marc", "Marina", "Puig", hashSetGossos, 'M', 20 );
        
        
        HashSet<Propietari> hashPropietaris2 = new HashSet<>();
        
        hashPropietaris2.add(p5);
        hashPropietaris2.add(p6);
        hashPropietaris2.add(p7);
        hashPropietaris2.add(p8);
        
        System.out.println("Implementacio 4");
        for(Propietari pro: hashPropietaris2){
            System.out.println(pro);
        }
        
        HashMap<Gos, Propietari> hash = new HashMap<>();
        
        hash.put(g, p);
        hash.put(g2, p2);
        hash.put(g3, p3); 
        
        //gos diferent mateix propietari
        hash.put(g4, p3);
        //Guarda el propietari, perque la clau(gos) es diferent, i un hashMap pot tindre valors repetits
        
        //mateix gos diferent propietari
        hash.put(g4,p2);
        //No afegeix el la entrada k, v perque la clau esta repetida
        
        
        hash.put(g5, new Propietari("34132123", "Ferran", "efqef", "wefw", hashSetGossos, 'M', 0));
        //Guarda el propietari, perque la clau(gos) es diferent, i un hashMap pot tindre valors repetits
        
        hash.put(g5,p2);
        //No afegeix el la entrada k, v perque la clau esta repetida
        
        System.out.println("-Proves-");
        hash.forEach((k,v) -> System.out.println(k + " " + v));
        
        
        //EXERCICI 7
        
        HashSet<LegoSet> hashSetLego = new HashSet<>();
        
        LegoSet ls = new LegoSet(10234, "Destructor", 4, 1200, 299.99, "Star Wars", new Date());
        LegoSet ls2 = new LegoSet(1234, "Zatcueva", 2, 300, 299.99, "Star Wars", new Date());
        LegoSet ls3 = new LegoSet(1234, "Batcueva", 4, 300, 299.99, "b", new Date());
        LegoSet ls4 = new LegoSet(2345, "Ferrari", 1, 200, 59.99, "Cotxe", new Date());
        
        hashSetLego.add(ls);
        hashSetLego.add(ls2);
        hashSetLego.add(ls3);
        hashSetLego.add(ls4);
        
        System.out.println("LEGOS");
        for(LegoSet l: hashSetLego){
            System.out.println(l);
        }
        
        SortedSet<LegoSet> sortedSet = new TreeSet<>();
        
        sortedSet.add(ls);
        sortedSet.add(ls2);
        sortedSet.add(ls3);
        
        System.out.println("hola");
        for(LegoSet l2: sortedSet){
            System.out.println(l2);
        }
        
        ArrayList<LegoSet> legos = new ArrayList<>();
        
        legos.add(ls);
        legos.add(ls2);
        legos.add(ls3);
        legos.add(ls4);
        
        System.out.println("LEGOS2");
        for(LegoSet l: legos){
            System.out.println(l);
        }
        
        Collections.sort(legos);
        
        System.out.println("LEGOS3");
        for(LegoSet l: legos){
            System.out.println(l);
        }
        
        SortedSet<LegoSet> sortedLegos2 = new TreeSet<>();
        
        sortedLegos2.add(ls3);
        sortedLegos2.add(ls);
        sortedLegos2.add(ls4); 
        sortedLegos2.add(ls2);
        
        System.out.println("Sorted Legos");
        for(LegoSet l: sortedLegos2){
            System.out.println(l);
        }
        
        //Prova compareTo nom i data
        Collections.sort(legos);
        
        System.out.println("LEGOS3");
        for(LegoSet l: legos){
            System.out.println(l);
        }
        
        //El comparator rep dos objectes com a parametre, i igual que el comparable, retorna un int segons la resta dels valors del atribut segons compara
        
        //Proves LegoSetDificultatComparator
        
        Collections.sort(legos, new LegoSetDificultatComparator());
        
        System.out.println("Proves LegoSetDificultatComparator");
        for(LegoSet l: legos){
            System.out.println(l);
        }
        
        //Proves LegoSetChaosComparator
        
        Collections.sort(legos, new LegoSetChaosComparator());
        
        System.out.println("Proves LegoSetChaosComparator");
        for(LegoSet l: legos){
            System.out.println(l);
        }
    }
    
}
