/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenantobjectes4;

import java.util.Date;

/**
 *
 * @author Ferran
 */
public class Gos {
    private String codiXip;
    private String nom_complet;
    private String nom; 
    private String raça;
    private boolean tePedigree;
    private Date dataNaixement;

    public Gos(String codiXip, String nom_complet, String nom, String raça, boolean tePedigree, Date dataNaixement) {
        this.codiXip = codiXip;
        this.nom_complet = nom_complet;
        this.nom = nom;
        this.raça = raça;
        this.tePedigree = tePedigree;
        this.dataNaixement = dataNaixement;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(); 
        
        sb.append("CodiXIP: ").append(codiXip).append(" Nom complet: ").append(nom_complet).append(" nom: ").append(nom).append(" raça: ")
                .append(raça).append(" pedigree: ").append(tePedigree).append(" data naixement: ").append(dataNaixement);
        
        return sb.toString();
    }
    
    
    
//    @Override
//    public int hashCode(){
//        int hash = 37; 
//        hash = 173 * hash + this.codiXip.hashCode();
//        return hash;
//    }
//
//
//    @Override
//    public boolean equals(Object o){
//        if(o == this){
//            return true;
//        }
//        else if(o == null || this.getClass() != o.getClass()){
//            return false;
//        }else{
//            Gos g = (Gos) o;
//            return this.codiXip.equals(g.codiXip);
//        }
//    }
    
    
    @Override
    public int hashCode(){
        int hash = 37; 
        hash = 173 * hash + this.nom.hashCode();
        hash = 173 * hash + this.raça.hashCode();
        return hash; 
    }
    
    @Override
    public boolean equals(Object o){
        if(o == this){
            return true;
        }
        else if(o == null || this.getClass() != o.getClass()){
            return false;
        }else{
            Gos g = (Gos) o;
            return this.raça.equals(g.raça) && this.nom.equals(g.nom);
        }
    }

    public String getCodiXip() {
        return codiXip;
    }
    
    
//    @Override
//    public int hashCode(){
//        int hash = 37; 
//        hash = 173 * hash + this.nom.hashCode();
//        hash = 173 * hash + this.raça.hashCode();
//        hash = 173 * hash + this.dataNaixement.hashCode();  
//        return hash; 
//    }
    
    //no cal comprovar que el pedigree sigui diferent
//    @Override
//    public boolean equals(Object o){
//        if(o == this){
//            return true;
//        }
//        else if(o == null || this.getClass() != o.getClass()){
//            return false;
//        }else{
//            Gos g = (Gos) o;
//            if(this.nom.equals(g.nom)){
//                if(this.raça.equals(g.raça)){
//                    if(this.dataNaixement.equals(g.dataNaixement)){
//                        return true;
//                    }
//                }
//            }
//        }
//        return false;
//    }
    
    
}
