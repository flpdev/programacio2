/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenantobjectes4;

import java.util.Comparator;

/**
 *
 * @author Ferran
 */
public class LegoSetDificultatComparator implements Comparator<LegoSet>{

    @Override
    public int compare(LegoSet o1, LegoSet o2) {
        int num = o1.getDificultat() - o2.getDificultat();
        
        if(num == 0){
            num = o2.getCategoria().toLowerCase().compareTo(o1.getCategoria().toLowerCase());
        }
        
        return num;
    }

   
    
}
