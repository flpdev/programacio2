/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenantobjectes4;

import java.util.Comparator;

/**
 *
 * @author Ferran
 */
public class LegoSetChaosComparator implements Comparator<LegoSet>{

    @Override
    public int compare(LegoSet o1, LegoSet o2) {
        int num = o1.getNumPeces() - o2.getNumPeces();
        if(num == 0){
            num = o2.getDificultat() - o1.getDificultat();
        }
        if(num == 0){
            num = o2.getDataLlansament().compareTo(o1.getDataLlansament());
        }
        
        return num;
    }
    
}
