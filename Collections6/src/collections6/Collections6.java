/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collections6;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author Ferran
 */
public class Collections6 {

    /**
     * @param args the command line arguments
     */
    
    private static HashMap<String, String> diccionari = new HashMap<>();
    
    public static void main(String[] args) {
        
        
        int opcio;
        do{
            opcio = menu();
            switch(opcio){
                case 1:
                    afegirEntrada(); 
                    break;
                case 2: 
                    consultarEntrada(); 
                    break;
                case 3: 
                    eliminarEntrada();
                    break;
                case 4: 
                    mostrarEntrades();
                    break;
                    
            }
                    
        }while(opcio != 0);
        
    }
    
    
    public static int menu(){
        int opcio=-1;
        
        System.out.println("1.-Afegir entrada");
        System.out.println("2.-Consultar defincio");
        System.out.println("3.-Eliminar definicio");
        System.out.println("4.-Mostrar totes les definicions");
        System.out.println("0.-Sortir"); 
        

        opcio = new Scanner(System.in).nextInt();
        return opcio;
        
    }
    
    public static void afegirEntrada(){
        //Si afegeixes un valor amb la mateixa key, es sobrescriu el valor anterior.
        System.out.println("Paraula: ");
        String k = new Scanner(System.in).nextLine();
        System.out.println("Definicio: ");
        String v = new Scanner(System.in).nextLine();
        
        diccionari.put(k, v);
        
    }
    
    public static void consultarEntrada(){
        System.out.println("Paraula: ");
        String k = new Scanner(System.in).nextLine();
        String d = diccionari.get(k);
        System.out.println("Paraula: " + k + " definicio: "+ d);
    }
    
    public static void eliminarEntrada(){
        System.out.println("Paraula: ");
        String k = new Scanner(System.in).nextLine();
        
        diccionari.remove(k);
    }
    
    public static void mostrarEntrades(){
        //La part comentada fa el mateix que la unica linia, crec que es lo de la lambda expression 
        
        diccionari.forEach((k,v) -> System.out.println("Paraula: " + k + " definicio: "+ v));
        
//        Iterator it = diccionari.entrySet().iterator();
//        while(it.hasNext()){
//            Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
//            System.out.println("Paraula: " + entry.getKey() + " definicio: "+ entry.getValue());
//        }
    }
            
}
