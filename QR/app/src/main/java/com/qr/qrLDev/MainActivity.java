package com.qr.qrLDev;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;


public class MainActivity extends AppCompatActivity implements ActionBar {

    SurfaceView cameraPreview;
    BarcodeDetector barcodeDetector;
    CameraSource cameraSource;
    private static final int requestCameraPermission = 1;
    private static final int INTENT_RESULT = 2;
    boolean vibrar = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar.hideActionBar(this);
        setContentView(R.layout.activity_main);

        cameraPreview = findViewById(R.id.cameraPreview);


        //Permissions check
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            // Si executem la versió Marshmallow (6.0) o posterior, haurem de demanar
            // permisos en temps d'execució

            // Comprovem si l'usuari ja ens ha donat permisos en una execió anterior
            if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                // Si l'usuari no ens havia atorgat permisos, els hi demanem i
                // executem el nostre codi

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.CAMERA},
                        requestCameraPermission);

                // Codi que volem executar
            } else {
                detectQRCode();
            }
        } else {

            detectQRCode();
        }

    }


    //Main QR Scanner functionality
    public void detectQRCode() {

        //Instantiation of BarcodeDetector
        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();


        //Retrieving the display size
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;


        cameraSource = new CameraSource.Builder(this, barcodeDetector).setRequestedPreviewSize(height, width).setAutoFocusEnabled(true).build();

        cameraPreview.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                initiateCameraSource();

            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                SparseArray<Barcode> qrcodes = detections.getDetectedItems();
                if (qrcodes.size() != 0) {
                    int i = qrcodes.keyAt(0);
                    SparseArray<Barcode> barcodes = new SparseArray<>();
                    barcodes.put(i, qrcodes.get(i));

                    if (barcodes.size() != 0) {

                        //One-time vibrate
                        if (vibrar) {
                            Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vibrator.vibrate(100);
                        }

                        vibrar = false;
                        String link = String.valueOf(barcodes.valueAt(0).displayValue);

                        if(barcodes.valueAt(0).rawValue.startsWith("BEGIN:VCARD")){

                            //Create contact

                            ContactHelper contactHelper = new ContactHelper(barcodes.valueAt(0).rawValue);
                            Contact contact =contactHelper.generateContact();
                            System.out.println(contact);

                            Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                            intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                            intent.putExtra(ContactsContract.Intents.Insert.NAME, contact.getName())
                                    .putExtra(ContactsContract.Intents.Insert.PHONE, contact.getMobile())
                                    .putExtra(ContactsContract.Intents.Insert.SECONDARY_PHONE, contact.getTelephone())
                                    .putExtra(ContactsContract.Intents.Insert.EMAIL, contact.getEmail());

                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                            startActivityForResult(intent, INTENT_RESULT);
                            cameraSource.stop();

                        }else if(barcodes.valueAt(0).rawValue.startsWith("http")){
                            //Navigate to new intent
                            Intent view = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                            view.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivityForResult(view, INTENT_RESULT);
                            cameraSource.stop();
                        }

                        //Stop cameraSource service
                        //cameraSource.stop();


                    }
                }

            }
        });
    }

    //Initiation of CameraSource
    public void initiateCameraSource() {

        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                cameraSource.start(cameraPreview.getHolder());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    // ------------ OVERRIDING ----------------------------

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case requestCameraPermission:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initiateCameraSource();
                    detectQRCode();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == INTENT_RESULT) {
            initiateCameraSource();
        }
    }

    @Override
    public void onBackPressed() {
        vibrar = true;
        detectQRCode();
        super.onBackPressed();
    }


    @Override
    protected void onResume() {
        vibrar = true;
        initiateCameraSource();
        detectQRCode();
        super.onResume();
    }





}
