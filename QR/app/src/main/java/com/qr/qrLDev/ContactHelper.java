package com.qr.qrLDev;

public class ContactHelper {

    private String data;

    public ContactHelper(String rawQRCodeData){
        this.data =  rawQRCodeData;
    }

    public Contact generateContact(){
        Contact contact = new Contact();

        String[] values = data.split("\n");

        contact.setName(getName(values));
        //contact.setLastName(getLastName(values));
        contact.setTelephone(getTel(values));
        contact.setMobile(getMobile(values));
        contact.setFax(getFax(values));
        contact.setOrganization(getOrg(values));
        contact.setURL(getUrl(values));
        contact.setEmail(getEmail(values));
        //contact.setTitle(title);

        return contact;
    }

    public String getName(String[] values){
        try{
            String name = values[3].split(":")[1];
            return name;
        }catch(ArrayIndexOutOfBoundsException ex){
            return "";
        }
    }

    public String getLastName(String[] values){
        String names = values[2].substring(2);
        try{
            String lastName = names.split(";")[0];
            return lastName;
        }catch(ArrayIndexOutOfBoundsException ex){
            return "";
        }
    }

    public String getOrg(String[] values){
        try{
            String ORG = values[4].substring(3);
            return ORG;
        }catch(ArrayIndexOutOfBoundsException ex){
            return "";
        }
    }

    public String getMobile(String[] values){
        try{
            String mobile = values[8].split(":")[1];
            return mobile;
        }catch(ArrayIndexOutOfBoundsException ex){
            return "";
        }
    }


    public String getFax(String[] values){
        try{
            String fax = values[9].split(":")[1];
            return fax;
        }catch(ArrayIndexOutOfBoundsException ex){
            return "";
        }
    }


    public String getEmail(String[] values){
        try{
            String email = values[10].split(":")[1];
            return email;
        }catch(ArrayIndexOutOfBoundsException ex){
            return "";
        }
    }

    public String getUrl(String[] values){
        try{
            String url = values[11].substring(3);
            return url;
        }catch(ArrayIndexOutOfBoundsException ex){
            return "";
        }
    }

    public String getTitle(String[] values){
        try{
            String title = values[5].split(":")[1];
            return title;
        }catch(ArrayIndexOutOfBoundsException ex){
            return "";
        }
    }

    public String getTel(String[] values){
        try{
            String tempTel = values[7];
            String tel = tempTel.split(":")[1];
            return tel;
        }catch(ArrayIndexOutOfBoundsException ex){
            return "";
        }
    }

}
