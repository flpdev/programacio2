/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queue.stack2;

import java.util.Stack;

/**
 *
 * @author Ferran
 */
public class QueueStack2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String ex = "123*+4-";
        System.out.println("Resultat de "+ ex +": "+ calcul(ex));
    }
    
    public static int calcul(String expresio){
        
        char[] caracters = expresio.toCharArray();
        Stack<Integer> stack = new Stack();
        int resultat = 0; 
        
        for(Character c: caracters){
           if(c!='+' && c!='-' && c!='/' && c!='*'){
                stack.push(Integer.parseInt(c.toString()));
            }else{
                if(stack.size() >= 2){
                    int op1 = stack.pop();
                    int op2 = stack.pop(); 
                    
                    switch(c){
                        case '+':
                            stack.push(op2+op1);
                            break; 
                        case '-':
                            stack.push(op2-op1); 
                            break;
                        case '/':
                            stack.push(op2/op1);
                            break;
                        case '*':
                            stack.push(op2*op1);
                            break;
                    }
                    
                }
                
            }
        }
        
        if(stack.size() ==1){
            resultat = stack.pop();
        }
        
        
        return resultat;
    }
}
