package com.xml;

import com.xml.dao.CityDAO;
import com.xml.dao.CountryDAO;
import com.xml.dao.CountryLanguageDAO;
import com.xml.managers.ConnectionManager;
import com.xml.pojo.Country;
import com.xml.pojo.CountryLanguage;

import java.sql.*;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;

        try {

            //1.a
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

//            connection = ConnectionManager.getConnection();

//            City city;

            //1.b
//            System.out.println("Nom de la city: ");
//            String nomCiutat = new Scanner(System.in).nextLine();


//            statement = connection.createStatement();
//            resultSet = statement.executeQuery("SELECT * FROM city WHERE name = '" + nomCiutat + "'");
//
//            if(!resultSet.isBeforeFirst()){
//                System.out.println("No s'ha trobat");
//            }else{
//                while (resultSet.next()) {
//                    city = CityParser.parseCity(resultSet);
//                    System.out.println(city);
//                }
//            }

            //1.c
            /*Consisteix en aprofitar que les queries estan concatenades per afegir enlloc d'un parametre valid
             un string que sigui una part d'una query sql, perque la consulta retorni per exemple
             totes les entrades de la base de dades enlloc de nomes el resultat desitjat*/

            //1.d  Kabul' or name != 'Kabul
            //1.e Kabul' or CountryCode = 'ESP
            //1.f Kabul' delete * from city where name != 'deafada
            //no es pot, perque per executar un delete cal fer un statement.executeUpdate()

            //PreparedStatement pstmt = con.prepareStatement("UPDATE EMPLOYEES
            //                                     SET SALARY = ? WHERE ID = ?");
            //   pstmt.setBigDecimal(1, 153833.00)
            //   pstmt.setInt(2, 110592)

            //1.g
//            preparedStatement = connection.prepareStatement("SELECT * FROM city WHERE name = ?");
//            preparedStatement.setString(1, nomCiutat);
//            resultSet = preparedStatement.executeQuery();
//
//            if(!resultSet.isBeforeFirst()){
//                System.out.println("No s'ha trobat");
//            }else{
//                while (resultSet.next()) {
//
//                    city = CityParser.parseCity(resultSet);
//                    System.out.println(city);
//                }
//            }

            //Bloc 2
            //El singleton es un patro de disseny que ens permet restringir la creació d'objectes d'una classe a 1
            //Ara nomes podrem tindre una conexio, evitant que es crein mes conexions.

            //3.d El dao ens permet tindre e una clase el CRUD d'una taula de la BD, aixi separem el acces a dades de la BD

            //Proves
//            ArrayList<City> cities = CityDAO.findByName("Kaabul");
//            City city2 = CityDAO.find(12345678);
//            if(cities != null){
//                for(City c: cities){
//                    System.out.println(c);
//                }
//
//            }
//            if(city2 != null){
//                System.out.println(city2);
//            }
//
//            City city3 = new City(null, "Ferran", "FLP", "Lopez", 300);
//            City city4 = new City(1, "Ferran", "FLP", "Lopez", 300);
//            //Comentat per comoditat
//            //CityDAO.saveOrUpdate(city3);
//            //CityDAO.saveOrUpdate(city4);
//
//            int num = CityDAO.delete(4083);
//            if(num > 0){
//                System.out.println("Ciutat borrada");
//            }else{
//                System.out.println("No s'ha pogut borrar");
//            }

            //El update es podria fer passant el objecte city i actualitzant la entrada que tingui el ID que te el objecte city passat com a parametre
            //Es podria fer un find generic, mirant quin tipus de valor es, per exemple amb un regex, i actuant en consequencia.

            //WORLD 3


            //BLOC2
//            Country country = CountryDAO.find("ABW");
//            if(country != null){
//                System.out.println(country);
//            }
//            else{
//                System.out.println("No s'ha trobat el pais");
//            }
//
//            //2.b
//            ArrayList<City> cities2 = CityDAO.findByCountryName("Spain");
//            for(City c: cities2){
//                System.out.println(c);
//            }
//
//            //2.c
//            ArrayList<CountryLanguage> countryLanguages = CountryDAO.findCountryLanguagesByCountryCode("ESP");
//            for(CountryLanguage c: countryLanguages){
//                System.out.println(c);
//            }


//            CountryDAO.deleteMostSpokenLanguage("ESP");

            //1.4, 1.a
            Country country = new Country("ABC", "PAIS", "OCEANIA", 2.33F, 120, 1);
            CountryDAO.saveOrUpdate(country);
            Country country2 = new Country("ABC", "PAIS2", "AMERICA, FUCK YEAH", 2.33F, 120, 1);
            CountryDAO.saveOrUpdate(country2);

//            //1.b
           CountryDAO.deleteCountry(country2);

            //1.c
            ArrayList<Country> countries = CountryDAO.CountriesByPopulationRange(10000, 20000);
            for(Country c: countries){
                System.out.println(c);
            }

            //1.d

            ArrayList<Country> countries3 = CountryLanguageDAO.getPaisosParlant("Spanish");
            for(Country c: countries3){
                System.out.println(c);
            }


            //1.e
            ArrayList<Country> countries2 = CountryDAO.getCountriesByRegionAndContinent("Caribbean","North America");
            for(Country c: countries2){
                System.out.println(c);
            }

            //1.f
            System.out.println("Numero de ciutats: " + CountryDAO.getNumeroCiutats("ESP"));

            //BLOC 2

            //Un batch es un llistat de statements o prepared statements que s'executaran tots alhora
            //Ens permeten executar queries simultaniament, si fem autocommit false mes eficaçment que si ho fessim query a query
            //Delete, update, insert

            CityDAO.updatePopulations();

            //Save point
            //Es com un punt de guardat xd. Ens permet tornar a un estat anterior de la transaccio sense fer rollback de la transaccio sencera
            //L'he implementat a CountryDao.deleteMostSpokenLanguage

            ConnectionManager.closeConnection();

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }finally {
            try {
                if (resultSet != null) resultSet.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }

            try {
                if ( preparedStatement != null) preparedStatement.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }


    }
}
