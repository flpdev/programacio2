package com.xml.managers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface DBHelper {

    static void tancarObjectesQueries(ResultSet resultSet, PreparedStatement preparedStatement){


        try {
            if (resultSet != null) resultSet.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        try {
            if (preparedStatement != null) preparedStatement.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        //ConnectionManager.closeConnection();

    }
}
