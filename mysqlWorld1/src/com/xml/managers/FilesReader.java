package com.xml.managers;

import java.io.*;
import java.util.ArrayList;

public abstract class FilesReader {

    public static ArrayList<String> getCountryCodes2Update(){
        ArrayList<String> countryCodes = new ArrayList<>();

        File file = new File("files\\CountryCodesToUpdate.txt");
        FileReader fr = null;
        BufferedReader br = null;

        try {
            if(file.exists()){
                fr = new FileReader(file);
                br = new BufferedReader(fr);

                String line = br.readLine();

                while(line != null){
                    countryCodes.add(line);
                    line = br.readLine();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fr != null){
                try {
                    fr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(br != null){
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return countryCodes;
    }
}
