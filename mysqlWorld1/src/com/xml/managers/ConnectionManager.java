package com.xml.managers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

    private static Connection connection;
    private static String connUrl = "jdbc:mysql://localhost:3306/dam2tworld?useTimezone=true&serverTimezone=UTC";
    private static String user = "root";
    private static String password = "root";

    public static Connection getConnection(){
        if(connection == null){

            try {
                connection = DriverManager.getConnection(connUrl, user, password);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    private ConnectionManager(){

    }

    public static void closeConnection(){

        try {
            if(connection!=null){
                connection.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
