package com.xml.dao;



import com.xml.managers.ConnectionManager;
import com.xml.managers.DBHelper;
import com.xml.parsers.CountryParser;
import com.xml.pojo.Country;
import java.sql.ResultSet;
import com.xml.pojo.CountryLanguage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class CountryLanguageDAO {

    public static int delete(CountryLanguage countryLanguage){
        int num = -1;
        Connection connection;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM CountryLanguage WHERE CountryCode = ? AND Language = ?");
            preparedStatement.setString(1, countryLanguage.getCountrycode());
            preparedStatement.setString(2, countryLanguage.getLanguage());
            num = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (preparedStatement != null) preparedStatement.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }

        }

        return num;
    }

    public static int updateIsOfficial(CountryLanguage countryLanguage) {
        int num = -1;
        Connection connection;
        PreparedStatement preparedStatement = null;

        try {

            connection = ConnectionManager.getConnection();

            preparedStatement = connection.prepareStatement("UPDATE countryLanguage SET IsOfficial = 'T' WHERE CountryCode = ? AND Language = ?");

            preparedStatement.setString(1, countryLanguage.getCountrycode());
            preparedStatement.setString(2, countryLanguage.getLanguage());

            num = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (preparedStatement != null) preparedStatement.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }

        return num;
    }

    public static ArrayList<Country> getPaisosParlant(String language){
        ArrayList<Country> countries = new ArrayList<>();
        Connection connection;
        PreparedStatement preparedStatement = null;
        ResultSet resultset = null;

        try{
            connection = ConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement("SELECT Country.* FROM Country" +
                    " JOIN CountryLanguage ON Country.Code = CountryLanguage.CountryCode " +
                    "WHERE CountryLanguage.Language = ?");

            preparedStatement.setString(1, language);

            resultset =  preparedStatement.executeQuery();

            if(resultset.isBeforeFirst()){
                while(resultset.next()){
                    countries.add(CountryParser.parseCountry(resultset));
                }
            }

        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBHelper.tancarObjectesQueries(resultset, preparedStatement);
        }

        return countries;
    }
}
