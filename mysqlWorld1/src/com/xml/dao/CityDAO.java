package com.xml.dao;

import com.xml.managers.ConnectionManager;
import com.xml.managers.DBHelper;
import com.xml.managers.FilesReader;
import com.xml.pojo.City;
import com.xml.parsers.CityParser;

import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;

public class CityDAO {

    public static City find(int id) {

        City city = null;
        Connection connection;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM city WHERE ID = ?");
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.isBeforeFirst()) {
                while (resultSet.next()) {

                    city = CityParser.parseCity(resultSet);
                    System.out.println(city);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) resultSet.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (preparedStatement != null) preparedStatement.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }

        return city;

    }

    public static ArrayList<City> findByName(String name) {

        ArrayList<City> cities = new ArrayList<>();
        Connection connection;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM city WHERE name = ?");
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.isBeforeFirst()) {
                while (resultSet.next()) {

                    Integer city_id = resultSet.getInt("ID");
                    String city_name = resultSet.getString("name");
                    String countryCode = resultSet.getString("CountryCode");
                    String district = resultSet.getString("district");
                    int population = resultSet.getInt("population");

                    City city = new City(city_id, city_name, countryCode, district, population);
                    cities.add(city);
                    //System.out.println(city);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) resultSet.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (preparedStatement != null) preparedStatement.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }

        return cities;

    }


    public static int saveOrUpdate(City city) {
        int num = -1;
        Connection connection;
        PreparedStatement preparedStatement = null;

        try {


            connection = ConnectionManager.getConnection();

            //UPDATE
            if (city.getId() != null) {
                preparedStatement = connection.prepareStatement("UPDATE city SET Name = ?, CountryCode = ?, District = ?, Population = ? WHERE ID = ?");
                //preparedStatement.setInt(1,city.getId());
                preparedStatement.setInt(5, city.getId());
                //INSERT
            } else {
                preparedStatement = connection.prepareStatement("INSERT INTO city VALUES (null,?,?,?,?)");
            }

            preparedStatement.setString(1, city.getName());
            preparedStatement.setString(2, city.getCountryCode());
            preparedStatement.setString(3, city.getDistrict());
            preparedStatement.setInt(4, city.getPopulation());

            num = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) preparedStatement.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }

        return num;
    }

    public static int delete(int id) {
        int num = -1;
        Connection connection;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM city WHERE ID = ?");
            preparedStatement.setInt(1, id);
            num = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) preparedStatement.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }

        }

        return num;
    }


    public static ArrayList<City> findByCountryName(String countryName) {


        ArrayList<City> cities = new ArrayList<>();
        City city = null;
        Connection connection;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement("SELECT city.* FROM city LEFT JOIN country on Country.Code = City.CountryCode WHERE Country.Name = ?");
            preparedStatement.setString(1, countryName);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.isBeforeFirst()) {
                while (resultSet.next()) {
                    city = CityParser.parseCity(resultSet);
                    cities.add(city);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) resultSet.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (preparedStatement != null) preparedStatement.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }

        return cities;
    }


    public static void updatePopulations(){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ArrayList<String> countryCodes = FilesReader.getCountryCodes2Update();

        int low = 2;
        int high = 7;
        try{
            connection = ConnectionManager.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement("UPDATE City SET Population = City.Population + City.Population * ? WHERE CountryCode = ?");

            for(String s: countryCodes){
                Random r = new Random();
                int result = r.nextInt(high-low) + low;
                float result2 = result / 100f;
                preparedStatement.setFloat(1, result2);
                preparedStatement.setString(2, s);
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();

        }catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                if(connection!= null){
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            DBHelper.tancarObjectesQueries(null, preparedStatement);
        }
    }
}
