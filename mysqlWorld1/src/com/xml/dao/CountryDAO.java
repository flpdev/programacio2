package com.xml.dao;

import com.xml.managers.ConnectionManager;
import com.xml.managers.DBHelper;
import com.xml.pojo.Country;
import com.xml.pojo.CountryLanguage;
import com.xml.parsers.CountryLanguageParser;
import com.xml.parsers.CountryParser;

import java.sql.*;
import java.util.ArrayList;

public class CountryDAO {

    public static Country find(String code) {

        Connection connection;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Country country = null;

        try {
            connection = ConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM Country WHERE code = ?");
            preparedStatement.setString(1, code);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                country = CountryParser.parseCountry(resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) resultSet.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (preparedStatement != null) preparedStatement.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }

        return country;
    }

    public static ArrayList<CountryLanguage> findCountryLanguagesByCountryCode(String countryCode) {

        ArrayList<CountryLanguage> countryLanguages = new ArrayList<>();
        Connection connection;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = ConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM CountryLanguage WHERE CountryCode = ?");
            preparedStatement.setString(1, countryCode);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.isBeforeFirst()) {
                while (resultSet.next()) {
                    CountryLanguage countryLanguage = CountryLanguageParser.parseCountryLanguage(resultSet);
                    countryLanguages.add(countryLanguage);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) resultSet.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (preparedStatement != null) preparedStatement.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }

        return countryLanguages;
    }

    public static int deleteMostSpokenLanguage(String countryCode) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        CountryLanguage countryLanguage = null;
        ResultSet resultSet = null;
        int num = -1;
        Savepoint savepointo = null;

        try {
            connection = ConnectionManager.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement("SELECT * FROM CountryLanguage WHERE countryCode = ? order by percentage DESC LIMIT 1");
            preparedStatement.setString(1, countryCode);
            resultSet = preparedStatement.executeQuery();

            savepointo = connection.setSavepoint();

            if (resultSet.isBeforeFirst()) {
                while (resultSet.next()) {

                    countryLanguage = CountryLanguageParser.parseCountryLanguage(resultSet);
                }
            }

            if (countryLanguage != null) {
                CountryLanguageDAO.delete(countryLanguage);
            }

            preparedStatement = connection.prepareStatement("SELECT * FROM CountryLanguage WHERE countryCode = ? order by percentage DESC LIMIT 1");
            preparedStatement.setString(1, countryCode);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.isBeforeFirst()) {
                while (resultSet.next()) {

                    countryLanguage = CountryLanguageParser.parseCountryLanguage(resultSet);
                }
            }

            CountryLanguageDAO.updateIsOfficial(countryLanguage);
            connection.commit();


        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.rollback(savepointo);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }

            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) resultSet.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (preparedStatement != null) preparedStatement.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }try{
                connection.setAutoCommit(true);
            }catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return num;
    }


    public static int saveOrUpdate(Country country) {
        Connection connection;
        PreparedStatement preparedStatement = null;
        int affectedRows = 0;
        try {

            Country country2 = CountryDAO.find(country.getCodi());
            connection = ConnectionManager.getConnection();

            if (country2 != null) {
                //UPDATE
                preparedStatement = connection.prepareStatement("UPDATE Country SET Name = ?, Region = ?, SurfaceArea = ?, Population = ?, Capital = ? WHERE Code = ?");
                preparedStatement.setString(1, country.getNom());
                preparedStatement.setString(2, country.getRegio());
                preparedStatement.setFloat(3, country.getSuperficie());
                preparedStatement.setInt(4, country.getPoblacio());
                preparedStatement.setInt(5, country.getCapital());
                preparedStatement.setString(6, country2.getCodi());
            } else {
                //INSERT
                preparedStatement = connection.prepareStatement("INSERT INTO country (Code, Name, Region, SurfaceArea, Population, Capital) VALUES (?, ?, ?,?,?,?)");
                preparedStatement.setString(1, country.getCodi());
                preparedStatement.setString(2, country.getNom());
                preparedStatement.setString(3, country.getRegio());
                preparedStatement.setFloat(4, country.getSuperficie());
                preparedStatement.setInt(5, country.getPoblacio());
                preparedStatement.setInt(6, country.getCapital());
            }

            affectedRows = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBHelper.tancarObjectesQueries(null, preparedStatement);
        }

        return affectedRows;
    }


    public static int deleteCountry(Country country) {
        Connection connection;
        PreparedStatement preparedStatement = null;
        int affectedRows = 0;
        try {
            connection = ConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM Country WHERE Code = ?");
            preparedStatement.setString(1, country.getCodi());
            affectedRows = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBHelper.tancarObjectesQueries(null, preparedStatement);
        }

        return affectedRows;
    }

    public static ArrayList<Country> CountriesByPopulationRange(int min, int max) {
        ArrayList<Country> countries = new ArrayList<>();
        Connection connection;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = ConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement("SELECT Country.* FROM Country " +
                    " JOIN City ON City.CountryCode = Country.Code " +
                    " WHERE City.Population >= ? AND City.Population <= ? " +
                    " GROUP BY Country.Code");
            preparedStatement.setInt(1,min);
            preparedStatement.setInt(2, max);
            resultSet = preparedStatement.executeQuery();

            if(resultSet.isBeforeFirst()){
                while(resultSet.next()){
                    countries.add(CountryParser.parseCountry(resultSet));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBHelper.tancarObjectesQueries(resultSet, preparedStatement);
        }
        return countries;
    }

    public static int getNumeroCiutats(String countryCode) {
        Connection connection;
        PreparedStatement preparedStatement = null;
        int numPaisos = 0;
        ResultSet resultSet = null;
        try {
            connection = ConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement("select count(CountryCode) AS total from City where CountryCode = ?");
            preparedStatement.setString(1, countryCode);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.isBeforeFirst()) {
                if (resultSet.next()) {
                    numPaisos = resultSet.getInt("total");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBHelper.tancarObjectesQueries(resultSet, preparedStatement);
        }

        return numPaisos;
    }

    public static ArrayList<Country> getCountriesByRegionAndContinent(String region, String continent){
        ArrayList<Country> countries =  new ArrayList<>();
        Connection connection;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try{
            connection = ConnectionManager.getConnection();
            preparedStatement =  connection.prepareStatement("SELECT * FROM Country WHERE Region = ? AND Continent = ?");
            preparedStatement.setString(1, region);
            preparedStatement.setString(2, continent);

            resultSet = preparedStatement.executeQuery();

            if(resultSet.isBeforeFirst()){
                while(resultSet.next()){
                    countries.add(CountryParser.parseCountry(resultSet));
                }
            }
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBHelper.tancarObjectesQueries(resultSet, preparedStatement);
        }

        return countries;
    }
}
