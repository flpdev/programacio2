package com.xml.pojo;

public class City {
    private Integer id;
    private String name;
    private String countryCode;
    private String district;
    private int population;

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append("ID: ").append(id).append(" name: ")
                .append(name)
                .append(" countryCode: ").append(countryCode)
                .append(" disrtict: ").append(district)
                .append(" population: ").append(population);

        return sb.toString();
    }

    public City(Integer id, String name, String countryCode, String district, int population) {
        this.id = id;
        this.name = name;
        this.countryCode = countryCode;
        this.district = district;
        this.population = population;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getDistrict() {
        return district;
    }

    public int getPopulation() {
        return population;
    }
}
