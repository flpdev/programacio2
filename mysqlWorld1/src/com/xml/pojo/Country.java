package com.xml.pojo;

public class Country {
    private String codi;
    private String nom;
    private String regio;
    private float superficie;
    private int poblacio;
    private int capital;

    public Country(String codi, String nom, String regio, float superficie, int poblacio, int capital) {
        this.codi = codi;
        this.nom = nom;
        this.regio = regio;
        this.superficie = superficie;
        this.poblacio = poblacio;
        this.capital = capital;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append("Codi: ").append(codi)
                .append(" Nom: ").append(nom)
                .append(" Regio: ").append(regio)
                .append(" Superficie: ").append(superficie)
                .append(" Poblacio: ").append(poblacio)
                .append(" Capital: ").append(capital);

        return sb.toString();
    }

    public String getCodi(){
        return this.codi;
    }

    public String getNom() {
        return nom;
    }

    public String getRegio() {
        return regio;
    }

    public float getSuperficie() {
        return superficie;
    }

    public int getPoblacio() {
        return poblacio;
    }

    public int getCapital() {
        return capital;
    }
}
