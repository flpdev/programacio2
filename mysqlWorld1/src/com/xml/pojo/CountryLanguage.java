package com.xml.pojo;

public class CountryLanguage {
    private String countrycode;
    private String language;
    private boolean isOfficial;
    private float percentatge;

    public CountryLanguage(String countrycode, String language, boolean isOfficial, float percentatge) {
        this.countrycode = countrycode;
        this.language = language;
        this.isOfficial = isOfficial;
        this.percentatge = percentatge;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append("CountryCode: ").append(countrycode)
                .append(" Language: ").append(language)
                .append(" isOfficial: ").append(isOfficial)
                .append(" percentatge: ").append(percentatge).append("%");

        return sb.toString();
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setIsOfficial(Boolean isOfficial){
        this.isOfficial = isOfficial;
    }

    public String getLanguage() {
        return language;
    }

    public boolean isOfficial() {
        return isOfficial;
    }

    public float getPercentatge() {
        return percentatge;
    }
}
