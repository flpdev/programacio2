package com.xml.parsers;

import com.xml.pojo.CountryLanguage;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CountryLanguageParser {

    public static CountryLanguage parseCountryLanguage(ResultSet resultSet){
        CountryLanguage countryLanguage = null;

        try{
            String countryCode = resultSet.getString("CountryCode");
            String language = resultSet.getString("Language");
            String isOfficialString = resultSet.getString("IsOfficial");
            boolean isOfficial;
            if(isOfficialString.equals("T")){
                isOfficial = true;
            }else{
                isOfficial=false;
            }
            float percentatge = resultSet.getFloat("Percentage");

            countryLanguage = new CountryLanguage(countryCode, language, isOfficial, percentatge);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return countryLanguage;
    }


}

