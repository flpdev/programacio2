package com.xml.parsers;

import com.xml.pojo.City;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CityParser {

    public static City parseCity(ResultSet resultSet){

        City city = null;

        try {
            Integer city_id = resultSet.getInt("ID");
            String name = resultSet.getString("name");
            String countryCode = resultSet.getString("CountryCode");
            String district = resultSet.getString("district");
            int population = resultSet.getInt("population");
            city = new City(city_id, name,countryCode, district, population);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return city;

    }
}
