package com.xml.parsers;

import com.xml.pojo.Country;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CountryParser {

    public static Country parseCountry(ResultSet resultSet) {

        Country country = null;

        try {

            String codi = resultSet.getString("Code");
            String nom = resultSet.getString("Name");
            String regio = resultSet.getString("Region");
            float superficie = resultSet.getFloat("SurfaceArea");
            int poblacio = resultSet.getInt("Population");
            int capital = resultSet.getInt("Capital");

            country = new Country(codi, nom, regio, superficie, poblacio, capital);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return country;

    }
}
