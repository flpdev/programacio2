package com.programa;

import com.sun.org.apache.xpath.internal.operations.Bool;

public class Propietari {
    private String nom;
    private String dni;
    private int edat;
    private boolean potConduir;

    public Propietari(String nom, String dni, int edat) {
        this.nom = nom;
        this.dni = dni;
        this.edat = edat;

        setEdat();
        setPotConduir();

    }

    public String getDni(){ return this.dni;}

    public void setEdat(){
        if(this.nom.equals("Marc Marina")) this.edat = -1;
    }

    public void setPotConduir(){
        if(this.edat>=18) potConduir = true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("Nom: ").append(nom).append(" dni: ")
                .append(dni).append( " Edat: ").append(edat).append(" pot conduir: ").append(potConduir).toString();
    }
}
