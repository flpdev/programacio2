package com.programa;


public class Cotxe {
    private String matricula;
    private int kilometres;
    private boolean donatDalta;
    private Propietari propietari;

    public Cotxe(String matricula, int kilometres, Propietari propietari, boolean donatDalta){
        this.matricula = matricula;
        this.kilometres = kilometres;
        this.propietari = propietari;
        this.donatDalta = donatDalta;
    }


    public Cotxe getCotxePropietari(Propietari propietari){
        if (propietari.getDni().equals(this.propietari.getDni())) return this;
        else return new Cotxe("", 0, new Propietari("", "", 0), false);
    }


    public void setPropietari(Propietari p){
        this.propietari = p;
    }


    @Override
    public String toString() {
        return new StringBuilder().append("Matricula: ").append(matricula).append(" Kilometres: ")
                .append(kilometres).append( " Alta: ").append(donatDalta).append(" PROPIETARI: ").append(propietari).toString();
    }

    public Propietari getPropietari() {
        return propietari;
    }

    public void accelerar(int gasolina){
        if (gasolina >= 0) this.kilometres += gasolina *2;
        else System.out.println("deixa de robar gasolina");
    }

    public int getKilometres(){
        return  this.kilometres;
    }
}
