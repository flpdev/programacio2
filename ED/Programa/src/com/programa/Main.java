package com.programa;


public class Main {

    public static void main(String[] args) {

        Propietari p = new Propietari("Marc Marina", "12345678A", 12);
        Propietari p2 = new Propietari("Antoni Marti", "87654321A", 22);
        Cotxe cotxe = new Cotxe("1232KHD", 2301, p,false);
        Cotxe cotxe2 = cotxe.getCotxePropietari(p2);

        Cotxe cotxe3 = cotxe.getCotxePropietari(p);
        for(int i=3; i>0; i--){
            cotxe.accelerar(i);
            cotxe2.accelerar(i+cotxe.getKilometres()/2);
        }


        if("".equals(cotxe2.getPropietari().getDni())){
            cotxe2.setPropietari(p2);
        }

        System.out.println(cotxe); System.out.println(cotxe2);
        System.out.println(cotxe3);
    }
}
