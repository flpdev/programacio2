package com.xml;

import com.xml.POJO.Articulo;
import com.xml.POJO.Fabricante;

import java.sql.*;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

            String connUrl = "jdbc:mysql://localhost:3306/tienda";
            String user = "root";
            String password = "root";

            conn = DriverManager.getConnection(connUrl, user, password);


            Articulo articulo = null;

            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM articulo where nombre like 'T%' AND Precio <= 110");

            while (rs.next()) {
                int cod_articulo = rs.getInt("Cod_articulo");
                String nombre = rs.getString("Nombre");
                int precio = rs.getInt("Precio");
                int stock = rs.getInt("Stock");
                int fabricante =  rs.getInt("Fabricante");

                articulo = new Articulo(cod_articulo,nombre,precio,stock,fabricante);

                System.out.println(articulo);
            }

            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM articulo where Precio >= 90 AND Precio <=150 order by Precio ASC");

            while (rs.next()) {
                int cod_articulo = rs.getInt("Cod_articulo");
                String nombre = rs.getString("Nombre");
                int precio = rs.getInt("Precio");
                int stock = rs.getInt("Stock");
                int fabricante =  rs.getInt("Fabricante");

                articulo = new Articulo(cod_articulo,nombre,precio,stock,fabricante);

                System.out.println(articulo);
            }

            //articulo.esborrarArticle(conn);

            Fabricante fabricante = null;

            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT cod_fabricante, nombre, NumEmpleados FROM fabricante where NumEmpleados > 200 order by Nombre ASC");

            while (rs.next()) {
                int cod_fabricante = rs.getInt("Cod_fabricante");
                String nombre = rs.getString("Nombre");
                int numEmpleados = rs.getInt("NumEmpleados");


                fabricante = new Fabricante(cod_fabricante, nombre, numEmpleados);

                System.out.println(fabricante);
            }

            fabricante.updateFabricant(conn, 5432);


            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT nombre, NumEmpleados FROM fabricante where nombre in ('Razer', 'Logitech', 'Kingston')");

            while (rs.next()) {
                //int cod_fabricante = rs.getInt("Cod_fabricante");
                String nombre = rs.getString("Nombre");
                int numEmpleados = rs.getInt("NumEmpleados");


                fabricante = new Fabricante( nombre, numEmpleados);

                System.out.println(fabricante);
            }

            Fabricante f = new Fabricante("prova",200);
            //f.afegirFabricant(conn);


            System.out.println(Articulo.findArticleByCodi(conn, 3));
        }catch (SQLException ex) {
            System.out.println(ex);
        } catch(ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            System.out.println(ex);
        } finally {
            // Cal tancar el ResultSet, el Statement i la Connection.
            // Estan en try-catch separats perquè si algun fallés s'intenti tancar la resta
            try {
                if (rs != null) rs.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (conn != null) conn.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }

    }
}
