package com.xml.POJO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Fabricante {
    private int cod_fabricante;
    private String nombre;
    private int numEmpleados;

    public Fabricante(int cod_fabricante, String nombre, int numEmpleados) {
        this.cod_fabricante = cod_fabricante;
        this.nombre = nombre;
        this.numEmpleados = numEmpleados;
    }

    public Fabricante(String nombre, int numEmpleados) {

        this.nombre = nombre;
        this.numEmpleados = numEmpleados;
    }

    @Override
    public String toString() {
        StringBuilder sb =  new StringBuilder();

        if(cod_fabricante != 0){
            sb.append("Codigo: ").append(cod_fabricante);
        }
        sb.append(" Nombre: ").append(nombre)
                .append(" Num empleados: ").append(numEmpleados);

        return sb.toString();

    }

    public void updateFabricant(Connection conn, int numEmp){
        try{
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("UPDATE fabricante SET NumEmpleados =" + numEmp);
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public void afegirFabricant(Connection conn){

        int num = 0;
        try{
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT MAX(cod_fabricante) FROM fabricante");

            while (rs.next()) {
                num = rs.getInt(1);
            }

            num++;

            stmt.executeUpdate("INSERT INTO fabricante VALUES ("+num+",'"+this.nombre+"',"+this.numEmpleados+")");
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
}
