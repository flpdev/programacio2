package com.xml.POJO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Articulo {
    private int cod_articulo;
    private String nombre;
    private int precio;
    private int stock;
    private int fabricante;

    public Articulo(int cod_articulo, String nombre, int precio, int stock, int fabricante) {
        this.cod_articulo = cod_articulo;
        this.nombre = nombre;
        this.precio = precio;
        this.stock = stock;
        this.fabricante = fabricante;
    }

    @Override
    public String toString() {
        StringBuilder sb =  new StringBuilder();

        sb.append("Codigo: ").append(cod_articulo).append(" Nombre: ").append(nombre)
                .append(" precio: ").append(precio).append(" stock: ").append(stock)
                .append(" fabricante: ").append(fabricante);

        return sb.toString();

    }

    public void esborrarArticle(Connection conn){

        try{
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("DELETE FROM articulo WHERE Cod_articulo = " + this.cod_articulo);
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public static Articulo findArticleByCodi(Connection conn, int id){
        Articulo articulo = null;

        try{
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM articulo where cod_articulo = "+id);

            while (rs.next()) {
                int cod_articulo = rs.getInt("Cod_articulo");
                String nombre = rs.getString("Nombre");
                int precio = rs.getInt("Precio");
                int stock = rs.getInt("Stock");
                int fabricante =  rs.getInt("Fabricante");

                articulo = new Articulo(cod_articulo,nombre,precio,stock,fabricante);

            }
        }catch(SQLException e){
            e.printStackTrace();
        }

        return articulo;
    }


}
