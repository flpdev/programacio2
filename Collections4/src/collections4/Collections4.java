/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collections4;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

/**
 *
 * @author Ferran
 */
public class Collections4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        HashSet<String> hs = new HashSet();
        
        hs.add("Hola");
        hs.add("Adeu");
        hs.add("Hello");
        hs.add("Bye");
        hs.add("Hola");
        hs.add("Hello");
        
        for(String s: hs){
            System.out.println(s);
        }
        
        //No s'afegeixen els elements repetits en el hashset
        
        HashSet<Persona> hsPersonas = new HashSet<>();
        Persona p1 = new Persona("Ferran", 22); 
        Persona p2 = new Persona("Antoni", 22);
        Persona p3 = new Persona("Ferran", 22); 
        
        hsPersonas.add(p1);
        hsPersonas.add(p1);
        hsPersonas.add(p2);
        hsPersonas.add(p3);
        
        for(Persona p: hsPersonas){
            System.out.println(p.toString());
        }
        
        //S'afegeix el objecte nou amb els mateixos valors, pero no deixa afegir els mateix objecte dos cops
        
        HashSet<Integer> hsIntegers = new HashSet();
        
        hsIntegers.add(1);
        hsIntegers.add(2);
        hsIntegers.add(3);
        hsIntegers.add(1);
        
        for(Integer i: hsIntegers){
            System.out.println(i);
        }
        
        //no accepta int, s'ha de parametritzar amb Integer
        
        HashSet<Character> hsChars = new HashSet();
        
        hsChars.add('A');
        hsChars.add('B');
        hsChars.add('C');
        hsChars.add('D');
        
        for(Character i: hsChars){
            System.out.println(i);
        }
        
        //No accepta els tipus primitius de variablaes (int, char) s'ha de parametritxzar amb la classe(Integer, String, Character...)
        
        LinkedList<String> hsStrings = new LinkedList<>();
        
        hsStrings.add("Bazinga!");
        hsStrings.add("String1");
        hsStrings.add("Bazinga!");
        hsStrings.add("String2");
        hsStrings.add("Bazinga!");
        hsStrings.add("String3");
        hsStrings.add("Bazinga!");
        hsStrings.add("String4");
        hsStrings.add("Bazinga!");
        
        hsStrings.add(0, "Element1");
        hsStrings.add(hsStrings.size(), "ElementFinal");
        hsStrings.remove(0);
        hsStrings.remove(hsStrings.size()-1);
        hsStrings.remove(3);
        System.out.println(hsStrings.indexOf("Bazinga!"));
        System.out.println(hsStrings.lastIndexOf("Bazinga!"));
        hsStrings.set(3, "valar moghulis");
        
        for(String s: hsStrings){
            System.out.println(s);
        }
        
        //No m'ha donat cap problema xD
        
        ArrayList<String> arrayListString = new ArrayList<>();
        
        arrayListString.add("Hola");
        arrayListString.add("Hello");
        arrayListString.add("Baguette");
        arrayListString.add("Corqueta");
        arrayListString.add("Gandalf esta sobrevalorat");
        //arrayListString.add("Hola");
        arrayListString.add("Corqueta");
        
        //Primera solucio --> Iterar el bucle dos vegades i buscar elements repetits
        boolean repetides = false;
        for (int i = 0; i < arrayListString.size(); i++) {
            String s = arrayListString.get(i);
            for (int j = i+1; j < arrayListString.size(); j++) {
                String g = arrayListString.get(j);
                if(g.equals(s)){
                    repetides = true;
                }
            }
        }
        
        if(repetides){
            System.out.println("Element repetits a la collection");
        }
        
        //Segona solucio --> Crear un ser, fer un addAll, i comparar tamany de les collections
        HashSet<String> hs1 = new HashSet<>();
        
        hs1.addAll(arrayListString); 
        
        if(hs1.size() != arrayListString.size()){
            System.out.println("Elements repetits a la collection");
        }
        
    }
    
}
