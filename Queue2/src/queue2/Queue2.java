/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queue2;

import java.util.LinkedList;

/**
 *
 * @author Ferran
 */
public class Queue2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        LinkedList<Integer> cua = new LinkedList<>();
        cua.add(1);
        cua.add(2);
        cua.add(3);
        cua.add(4);
        cua.add(5);
        
        LinkedList<Integer> res = duplicats(cua);
        
        for(Integer i: res){
            System.out.println(i);
        }
    }
    
    public static LinkedList<Integer> duplicats(LinkedList<Integer> cua){
        LinkedList<Integer> cuaResultat = new LinkedList<>();
        while(!cua.isEmpty()){
            int num = cua.pop();
            cuaResultat.add(num);
            cuaResultat.add(num);
        }
        
        return cuaResultat;
    }
    
    
    
}
