/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenanrobjectes3;

/**
 *
 * @author Ferran
 */
public class OrdenanrObjectes3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Alumne a = new Alumne("Ferran", "Lopez", 22);
        Alumne a2 = new Alumne("Ferran", "Lopez", 21);
        Alumne a3 = new Alumne("Antoni", "Lopez", 22);
        
        System.out.println(a.equals(a2));
        System.out.println(a.equals(a3));
    }
    
}
