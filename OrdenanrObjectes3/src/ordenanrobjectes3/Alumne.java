/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenanrobjectes3;



/**
 *
 * @author Ferran
 */
public class Alumne {
    private String nom;
    private String cognom; 
    private int edat;

    public static final int SEED = 173; 
    public static final int PRIME = 37; 

    public Alumne(String nom, String cognom, int edat) {
        this.nom = nom;
        this.cognom = cognom;
        this.edat = edat;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o){
            return true;
        }
        else if(o == null || o.getClass() != this.getClass()){
            return false; 
        }
        else{
            Alumne a = (Alumne) o; 
            return this.nom.equals(a.nom) && this.cognom.equals(a.cognom);
        }
    }   
}
