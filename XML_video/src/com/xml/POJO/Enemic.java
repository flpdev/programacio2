package com.xml.POJO;

public class Enemic {
    protected String colour;
    protected int HP;
    protected float avgDamage;
    protected String desc;

    public void setColour(String colour) {
        this.colour = colour;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }

    public void setAvgDamage(float avgDamage) {
        this.avgDamage = avgDamage;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "Enemic{" +
                "colour='" + colour + '\'' +
                ", HP=" + HP +
                ", avgDamage=" + avgDamage +
                ", desc='" + desc + '\'' +
                '}';
    }
}
