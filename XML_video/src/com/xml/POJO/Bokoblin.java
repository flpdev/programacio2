package com.xml.POJO;

import java.util.ArrayList;

public class Bokoblin extends Enemic {
    private String weapon;
    private Boolean shield;
    private ArrayList<String> drop;


    public Bokoblin(){
        this.drop = new ArrayList<>();
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    public Boolean getShield() {
        return shield;
    }

    public void setShield(Boolean shield) {
        this.shield = shield;
    }

    public ArrayList<String> getDrop() {
        return drop;
    }

    public void setDrop(ArrayList<String> drop) {
        this.drop = drop;
    }

    @Override
    public String toString() {
        return "Bokoblin{" +
                "weapon='" + weapon + '\'' +
                ", shield=" + shield +
                ", drop=" + drop +
                ", colour='" + colour + '\'' +
                ", HP=" + HP +
                ", avgDamage=" + avgDamage +
                ", desc='" + desc + '\'' +
                '}';
    }
}
