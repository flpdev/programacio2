package com.xml.POJO;

public class Octorok extends Enemic {
    private char tipus;

    public char getTipus() {
        return tipus;
    }

    public void setTipus(char tipus) {
        this.tipus = tipus;
    }

    @Override
    public String toString() {
        return "Octorok{" +
                "tipus=" + tipus +
                ", colour='" + colour + '\'' +
                ", HP=" + HP +
                ", avgDamage=" + avgDamage +
                ", desc='" + desc + '\'' +
                '}';
    }
}
