package com.xml.POJO;

public class Chuchu extends Enemic {
    private int dany_minim;
    private int dany_maxim;

    public void setDany_minim(int dany_minim) {
        this.dany_minim = dany_minim;
    }

    public void setDany_maxim(int dany_maxim) {
        this.dany_maxim = dany_maxim;
    }


    @Override
    public String toString() {
        return "Chuchu{" +
                "dany_minim=" + dany_minim +
                ", dany_maxim=" + dany_maxim +
                ", colour='" + colour + '\'' +
                ", HP=" + HP +
                ", avgDamage=" + avgDamage +
                ", desc='" + desc + '\'' +
                '}';
    }
}
