package com.xml.POJO;

import java.util.ArrayList;
import java.util.Date;

public class Acte {
    private long ID;
    private String nom;
    private String lloc;
    private String disctricte;
    private int municipi;
    private Date dataInici;
    private Date dataFI;
    ArrayList<Tematica> tematiques;



    public Acte(){
        this.tematiques = new ArrayList<>();
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLloc() {
        return lloc;
    }

    public void setLloc(String lloc) {
        this.lloc = lloc;
    }

    public String getDisctricte() {
        return disctricte;
    }

    public void setDisctricte(String disctricte) {
        this.disctricte = disctricte;
    }

    public int getMunicipi() {
        return municipi;
    }

    public void setMunicipi(int municipi) {
        this.municipi = municipi;
    }

    public Date getDataInici() {
        return dataInici;
    }

    public void setDataInici(Date dataInici) {
        this.dataInici = dataInici;
    }

    public Date getDataFI() {
        return dataFI;
    }

    public void setDataFI(Date dataFI) {
        this.dataFI = dataFI;
    }

    public ArrayList<Tematica> getTematiques() {
        return tematiques;
    }

    public void setTematiques(ArrayList<Tematica> tematiques) {
        this.tematiques = tematiques;
    }

    @Override
    public String toString() {
        return "Acte{" +
                "ID=" + ID +
                ", nom='" + nom + '\'' +
                ", lloc='" + lloc + '\'' +
                ", disctricte='" + disctricte + '\'' +
                ", municipi=" + municipi +
                ", dataInici=" + dataInici +
                ", dataFI=" + dataFI +
                ", tematiques=" + tematiques +
                '}';
    }
}
