package com.xml.POJO;

public class Tematica {

    private long codi;
    private String nom;

    public long getCodi() {
        return codi;
    }

    public void setCodi(long codi) {
        this.codi = codi;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Tematica{" +
                "codi=" + codi +
                ", nom='" + nom + '\'' +
                '}';
    }
}
