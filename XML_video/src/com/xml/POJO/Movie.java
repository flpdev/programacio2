package com.xml.POJO;

import java.util.ArrayList;

public class Movie {
    private String title;
    private int year;
    private String country;
    private String genre;
    private String summary;
    private Director director;
    private ArrayList<Actor> actors;


    public Movie(){
        actors  = new ArrayList<>();
    }
    public String getTitle() {
        return title;
    }


    public int getYear() {
        return year;
    }

    public String getCountry() {
        return country;
    }

    public String getGenre() {
        return genre;
    }

    public String getSummary() {
        return summary;
    }

    public Director getDirector() {
        return director;
    }

    public ArrayList<Actor> getActors() {
        return actors;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    public void setActors(ArrayList<Actor> actors) {
        this.actors = actors;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", year=" + year +
                ", country='" + country + '\'' +
                ", genre='" + genre + '\'' +
                ", summary='" + summary + '\'' +
                ", director=" + director +
                ", actors=" + actors +
                '}';
    }
}
