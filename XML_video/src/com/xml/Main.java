package com.xml;

import Utils.DateParser;
import com.xml.Parsers.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
	// write your code here
        SAXParserFactory spfac = SAXParserFactory.newInstance();
        SAXParser sp = null;

        try{
            sp = spfac.newSAXParser();
            WoWParser handler = new WoWParser();
            sp.parse("doc/WoW_Characters.xml", handler);
            handler.showCharacters();

            System.out.println( "--------------------");

            BooksParser handlerBooks = new BooksParser();
            sp.parse("doc/Books.xml", handlerBooks);
            handlerBooks.showBooks();

            System.out.println( "--------------------");

            MovieParser handlerMovies = new MovieParser();
            sp.parse("doc/Movies.xml", handlerMovies);
            handlerMovies.showMovies();

            System.out.println( "--------------------");

            ActesParser handlerActes = new ActesParser();
            sp.parse("doc/agendaEsportiva.xml", handlerActes);
            handlerActes.showActes();


            System.out.println( "--------------------");
            ZeldaParser handlerZelda = new ZeldaParser();
            sp.parse("doc/zeldaBreath.xml", handlerZelda);
            handlerZelda.showEnemies();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }
}
