package com.xml.Parsers;

import Utils.DateParser;
import com.xml.POJO.Acte;
import com.xml.POJO.Tematica;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.Iterator;

public class ActesParser extends DefaultHandler {
     private Acte acte;
     private ArrayList<Acte> actes = new ArrayList<>();
     private Tematica tematica;
     private int municipi;

    private StringBuilder temp = new StringBuilder();
    private String str;
    private char c;

    public void showActes(){
        Iterator it = actes.iterator();
         while(it.hasNext()){
             System.out.println(it.next());
         }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch (qName){
            case "acte":
                acte = new Acte();
                c = 'a';
                break;
            case "classificacions":
                tematica = new Tematica();
                c = 't';
                break;
            case "municipi":
                municipi = Integer.parseInt(attributes.getValue("codi"));
                break;
            case "nivell":
                tematica = new Tematica();
                tematica.setCodi(Long.parseLong(attributes.getValue("codi")));
                break;

            case "lloc_simple":
                c = 'l';
                break;

        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        str = temp.toString().trim();

        switch (qName){
            case "id":
                if(c == 'a'){
                    acte.setID(Long.parseLong(str));
                }
                break;
            case "nom":
                if(c == 'a'){
                    acte.setNom(str);
                }else if(c=='l'){
                    acte.setLloc(str);
                }
                break;
            case "municipi":
                acte.setMunicipi(municipi);
                break;

            case "districte":

                if(municipi == 19){
                    acte.setDisctricte(str);
                }

                break;


            case "data_inici":
                if(!temp.equals("")) {
                    acte.setDataInici(DateParser.String2Date(str, "dd/MM/yyyy"));
                }
                break;

            case "data_fi":
                if(!temp.equals("")) {
                    acte.setDataFI(DateParser.String2Date(str, "dd/MM/yyyy"));
                }
                break;

            case "nivell":
                tematica.setNom(str);
                acte.getTematiques().add(tematica);
                break;

            case "acte":
                actes.add(acte);
                break;


        }
        temp.setLength(0);
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        temp.append(new String(ch, start, length));
    }
}
