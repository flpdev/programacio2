package com.xml.Parsers;

import com.xml.POJO.Personaje;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.Iterator;

@SuppressWarnings("ALL")
public class WoWParser extends DefaultHandler {

    private String temp;
    private ArrayList<Personaje> pjs = new ArrayList<>();
    private Personaje pj;


    public void showCharacters(){
        Iterator iterator = pjs.iterator();

        while((iterator.hasNext())){
            System.out.println(iterator.next());
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if(qName.equals("Character")){
            pj = new Personaje();
            pj.setFaction(attributes.getValue("faction"));
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName){
            case "Name":
                pj.setName(temp);
                break;
            case "Class":
                pj.setClase(temp);
                break;
            case "Lvl":
                pj.setLvl(Integer.parseInt(temp));
                break;
            case "Character":
                pjs.add(pj);
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        temp = new String(ch, start, length);
    }


}
