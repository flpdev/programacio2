package com.xml.Parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import com.xml.POJO.*;

import java.util.ArrayList;
import java.util.Iterator;


public class MovieParser extends DefaultHandler {

    private Movie movie;
    private Director director;
    private Actor actor;


    private ArrayList<Movie> movies = new ArrayList<>();
    private StringBuilder temp = new StringBuilder();
    private String str;
    private char c;


    public void showMovies(){
        Iterator it = movies.iterator();

        while(it.hasNext()){
            System.out.println(it.next());
        }
    }
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        if(qName.equals("movie")){
            c = 'm';
            movie = new Movie();
        }else if(qName.equals("director")){
            c = 'd';
            director = new Director();
        }else if(qName.equals("actor")){
            c = 'a';
            actor = new Actor();
        }
        temp.setLength(0);

    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        str = temp.toString().trim();
        switch (qName){
            case "title":
                movie.setTitle(str);
                break;
            case "year":
                movie.setYear(Integer.parseInt(str));
                break;
            case "country":
                movie.setCountry(str);
                break;
            case "genre":
                movie.setGenre(str);
                break;

            case "summary":
                movie.setSummary(str);
                break;
            case "first_name":
                if(c == 'd'){
                    director.setFirst_name(str);
                }else if(c == 'a'){
                    actor.setFirst_name(str);
                }
                break;

            case "last_name":
                if(c == 'd'){
                    director.setLast_name(str);
                }else if(c == 'a'){
                    actor.setLast_name(str);
                }
                break;

            case "birth_date":
                if(c == 'd'){
                    director.setBirth_date(Integer.parseInt(str));
                } else if (c == 'a') {

                    actor.setBirth_date(Integer.parseInt(str));
                }
                break;

            case "role":
                actor.setRole(str);
                break;

            case "movie":
                movies.add(movie);
                break;

            case "director":
                movie.setDirector(director);
                break;

            case "actor":
                movie.getActors().add(actor);
                break;
        }

    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
            temp.append(new String(ch, start, length));
    }
}
