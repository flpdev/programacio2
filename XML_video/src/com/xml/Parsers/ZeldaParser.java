package com.xml.Parsers;

import com.xml.POJO.Bokoblin;
import com.xml.POJO.Chuchu;
import com.xml.POJO.Enemic;
import com.xml.POJO.Octorok;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

public class ZeldaParser extends DefaultHandler {

    private Bokoblin bokoblin;
    private Chuchu chuchu;
    private Octorok octorok;

    private int minDmg;
    private int maxDmg;

    private ArrayList<Enemic> enemics =  new ArrayList<>();
    private char c;
    private StringBuilder temp = new StringBuilder();
    private String str;


    public void showEnemies(){
        for(Enemic e: enemics){
            System.out.println(e);
        }

    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch (qName){
            case "Bokoblin":
                c = 'b';
                bokoblin = new Bokoblin();
                break;
            case "Chuchu":
                c = 'c';
                chuchu = new Chuchu();
                break;
            case "Octorok":
                octorok=new Octorok();
                c = 'o';
                break;

            case  "avgDamage":
                if(attributes.getValue("min") != null){
                    minDmg = Integer.parseInt(attributes.getValue("min"));
                    chuchu.setDany_minim(minDmg);
                }
                if(attributes.getValue("max") != null){
                    maxDmg = Integer.parseInt(attributes.getValue("max"));
                    chuchu.setDany_maxim(maxDmg);
                }

                break;
            }



    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        str = temp.toString().trim();

        switch (qName){
            case "weapon":
                bokoblin.setWeapon(str);
                break;
            case "shield":
                bokoblin.setShield(Boolean.parseBoolean(str));
                break;
            case "drop":
                String[] strings = str.split(",");
                for(int i =0;i<strings.length;i++){
                    bokoblin.getDrop().add(strings[i]);
                }
                break;

            case "type":
                octorok.setTipus(str.charAt(0));
                break;
            case "color":
                if(c=='c'){
                    chuchu.setColour(str);
                }else if(c=='o'){
                    octorok.setColour(str);
                }else if(c=='b'){
                    bokoblin.setColour(str);
                }
                break;

            case "HP":
                if(c=='c'){
                    chuchu.setHP(Integer.parseInt(str));
                }else if(c=='o'){
                    octorok.setHP(Integer.parseInt(str));
                }else if(c=='b'){
                    bokoblin.setHP(Integer.parseInt(str));
                }
                break;

            case "avgDamage":
                if(c =='c'){
                    chuchu.setAvgDamage(Float.parseFloat(str));
                    //chuchu.setDany_minim(minDmg);
                    //chuchu.setDany_maxim(maxDmg);
                }else if(c=='o'){
                    octorok.setAvgDamage(Float.parseFloat(str));
                }else if(c=='b'){
                    bokoblin.setAvgDamage(Float.parseFloat(str));
                }
                break;

            case "desc":
                if(c=='c'){
                    chuchu.setDesc(str);
                }else if(c=='o'){
                    octorok.setDesc(str);
                }else if(c=='b'){
                    bokoblin.setDesc(str);
                }
                break;
            case "Chuchu":
                enemics.add(chuchu);
                break;
            case "Octorok":
                enemics.add(octorok);
                break;
            case "Bokoblin":
                enemics.add(bokoblin);
                break;

        }

        temp.setLength(0);
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        temp.append(new String(ch, start, length));
    }
}
