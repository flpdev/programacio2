package com.xml.Parsers;

import Utils.DateParser;
import com.xml.POJO.Book;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.Iterator;

public class BooksParser extends DefaultHandler {

    //private String temp;
    private StringBuilder sb = new StringBuilder();
    private ArrayList<Book> books = new ArrayList<>();
    private Book book;


    public void showBooks(){
        Iterator it = books.iterator();
        while(it.hasNext()){
            System.out.println(it.next());
        }
    }
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if(qName.equals("book")){
            book = new Book();
            book.setId(attributes.getValue("id"));
        }
        sb.setLength(0);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName){
            case "author":
                book.setAuthor(sb.toString().trim());
                break;
            case "title":
                book.setTitle(sb.toString().trim());
                break;
            case "genre":
                book.setGenre(sb.toString().trim());
                break;
            case "price":
                book.setPrice(Double.parseDouble(sb.toString().trim()));
                break;
            case "publish_date":
                book.setPublish_date(DateParser.String2Date(sb.toString().trim(), "yyyy-MM-dd"));
                break;
            case "description":
                book.setDescription(sb.toString().trim());
                break;
            case "book":
                books.add(book);
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        sb.append(new String(ch, start, length));
    }
}
