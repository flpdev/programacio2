package Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateParser {


    private static String dateFormat = "dd.MM.yy";

    private static  SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

    public static Date String2Date(String date, String format) {


        sdf.applyPattern(format);
        Date d = null;

        try {
            d = sdf.parse(date);

        }catch(ParseException ex) {
            System.out.println(ex.toString());
        }

        return d;
    }

    public static Date String2Date(String date) {


        Date d = null;

        try {
            d = sdf.parse(date);

        }catch(ParseException ex) {
            System.out.println(ex.toString());
        }

        return d;
    }

    public static String Date2String(Date date, String format){

        String d;

        d = sdf.format(date);

        return d;
    }

    public static String Date2String(Date date){

        String d;

        d = sdf.format(date);

        return d;
    }


    public static void setDateFormat(String format){
        dateFormat = format;
    }

}
